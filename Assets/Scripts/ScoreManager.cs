using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TurnResolver turnResolver;
        public static ScoreManager instance { get; private set; }

        private Dictionary<PlayerTurn, int> scores = new Dictionary<PlayerTurn, int>();

        private void Start()
        {
            if (instance == null)
                instance = this;

            InitScores();
        }

        private void InitScores()
        {
            scores.Add(PlayerTurn.FirstPlayer, 0);
            scores.Add(PlayerTurn.SecondPlayer, 0);
        }

        public void AddScoreToCurrentPlayer()
        {
            var value = ++scores[turnResolver.Turn];
            UIManager.instance.AddScoresToPlayer(turnResolver.Turn, value);
        }
    }
}
