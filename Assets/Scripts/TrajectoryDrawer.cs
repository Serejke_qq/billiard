using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(LineRenderer))]
    public class TrajectoryDrawer : MonoBehaviour
    {
        private LineRenderer line;
        private List<Vector3> points;

        private void Start()
        {
            line = GetComponent<LineRenderer>();
            points = new List<Vector3>();
        }

        public void AddLine(Vector3 startPos, Vector3 endPos)
        {
            line.positionCount += 2;
            points.Add(startPos);
            points.Add(endPos);
        }

        public void AddLine(Vector3 p0, Vector3 p1, Vector3 p2)
        {
            line.positionCount += 3;
            points.Add(p0);
            points.Add(p1);
            points.Add(p2);
        }

        public void DrawPreparedLine()
        {
            line.SetPositions(points.ToArray());
        }

        public void RemoveLine()
        {
            line.positionCount = 0;
            points.Clear();
        }
    }
}
