namespace Assets.Scripts
{
    public class Tools
    {
        public static float Remap(float in1, float in2, float out1, float out2, float val)
        {
            return out1 + (val - in1) * (out2 - out1) / (in2 - in1);
        }
    }
}