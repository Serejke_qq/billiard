using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class GameRestarter : MonoBehaviour
    {
        private int ballsCounter;
        public static GameRestarter instance { get; private set; }

        void Start()
        {
            if (instance == null)
                instance = this;
            InitBallsCounter();
        }

        private void InitBallsCounter()
        {
            ballsCounter = GameObject.FindGameObjectsWithTag("Ball").Length;
            if (ballsCounter == 0)
                throw new Exception("Can't find any balls on the table!");
        }

        public void RemoveOneBallFromTable()
        {
            if (--ballsCounter == 0)
            {
                RestartCurrentScene();
            }
        }

        public void RestartCurrentScene()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
