using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(TrajectoryDrawer))]
    public class DragObjectShooter : MonoBehaviour
    {
        private Vector2 firstClickPos;
        private Vector2 secondClickPos;
        [SerializeField] private float maxObjectForce;

        [SerializeField] 
        [Range(1, 20)]
        [Tooltip("Distance between clicks when ball force will be maximum")]
        private float maxDesiredDistanceBetweenClicks;

        private Camera cam;
        private Rigidbody2D ballRigidBody;
        private TrajectoryDrawer trajectory;

        private float ballRadius;
        private bool readyToHitBall;

        [SerializeField] private GameObject ballHologramPrefab;
        private GameObject ballHologram;

        [SerializeField] private TurnResolver turnResolver;
        private float minSpeedThreshold = 0.125f;

        [SerializeField] private GameObject clickPointerPrefab;

        void Start()
        {
            cam = Camera.main;
            ballRigidBody = GetComponent<Rigidbody2D>();
            ballRadius = GetComponent<CircleCollider2D>().radius;
            trajectory = GetComponent<TrajectoryDrawer>();

            InitBallHologram();
            InitClickPointer();
        }

        private void InitBallHologram()
        {
            ballHologram = Instantiate(ballHologramPrefab, transform.position, Quaternion.identity);
            ballHologram.transform.localScale = transform.localScale;
            ballHologram.SetActive(false);
        }

        private void InitClickPointer()
        {
            clickPointerPrefab = Instantiate(clickPointerPrefab, transform.position, Quaternion.identity);
            clickPointerPrefab.SetActive(false);
        }

        void Update()
        {
            StopBallIfSpeedLessThan(minSpeedThreshold);

            if (turnResolver.IsCurrentPlayerAlreadyHitBall)
                return;
            
            if (Input.GetMouseButtonDown(0))
            {
                firstClickPos = cam.ScreenToWorldPoint(Input.mousePosition);
                clickPointerPrefab.transform.position = firstClickPos;
                clickPointerPrefab.SetActive(true);
                ballHologram.SetActive(true);
                readyToHitBall = true;
            }

            if (Input.GetMouseButton(0))
            {
                if (!readyToHitBall)
                    return;
                trajectory.RemoveLine();
                Vector2 currentMousePos = cam.ScreenToWorldPoint(Input.mousePosition);
                var lookDir = (firstClickPos - currentMousePos).normalized;
                var hit = Physics2D.CircleCast(transform.position, ballRadius * transform.localScale.x, lookDir);

                if (hit)
                {
                    ShowTrajectoryOfHit(lookDir, hit);
                }

                var hitForce = CalculateBallForceLogic(firstClickPos, currentMousePos);
                UIManager.instance.ChangePlayerForceTouch(hitForce);

                trajectory.DrawPreparedLine();
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (!readyToHitBall)
                    return;
                ballHologram.SetActive(false);
                clickPointerPrefab.SetActive(false);
                trajectory.RemoveLine();

                secondClickPos = cam.ScreenToWorldPoint(Input.mousePosition);
                KickBall(firstClickPos, secondClickPos);
                readyToHitBall = false;
            }
        }

        private void KickBall(Vector3 firstClick, Vector3 secondClick)
        {
            var hitDirection = (firstClick - secondClick).normalized;
            var resultForce = CalculateBallForceLogic(firstClick, secondClick);
            
            if (resultForce != 0)
                turnResolver.OnCurrentPlayerHitBall();

            ballRigidBody.velocity = hitDirection * resultForce;
        }

        private void ShowTrajectoryOfHit(Vector2 lookDir, RaycastHit2D hit)
        {
            Vector2 reflected = Vector3.Reflect(lookDir, hit.normal);
            trajectory.AddLine(transform.position, hit.centroid, hit.centroid + reflected);

            // should I use separate type for ball in the game? In this case not sure.. that's why here is tags
            if (hit.collider.CompareTag("Ball"))
            {
                trajectory.AddLine(hit.centroid,
                    hit.point + ((Vector2) hit.collider.transform.position - hit.point).normalized);
            }

            ballHologram.transform.position = hit.centroid;
        }

        private float CalculateBallForceLogic(Vector3 firstClick, Vector3 secondClick)
        {
            var distanceClamped = DeltaBetweenClicksClamped(firstClick, secondClick);
            var resultForce = Tools.Remap(0, maxDesiredDistanceBetweenClicks, 0, maxObjectForce, distanceClamped);
            return resultForce;
        }

        private float DeltaBetweenClicksClamped(Vector3 first, Vector3 second)
        {
            var distance = Vector2.Distance(first, second);
            var distanceClamped = Mathf.Clamp(distance, 0, maxDesiredDistanceBetweenClicks);
            return distanceClamped;
        }

        private void StopBallIfSpeedLessThan(float threshold)
        {
            // is ball already stopped?
            if(ballRigidBody.velocity.magnitude == 0)
                return;

            if (Mathf.Abs(ballRigidBody.velocity.x) < threshold && Mathf.Abs(ballRigidBody.velocity.y) < threshold)
            {
                ballRigidBody.velocity = Vector2.zero;
            }
        }
    }
}
