using UnityEngine;

namespace Assets.Scripts
{
    public class TurnResolver : MonoBehaviour
    {
        [SerializeField] private Rigidbody2D mainBallRb;

        public PlayerTurn Turn { get; private set; }
        public bool IsCurrentPlayerAlreadyHitBall { get; private set; }
        private bool turnWasChangedAlready;
        private bool additionalTurnForCurrentPlayer;

        private void Start()
        {
            turnWasChangedAlready = true;
            UIManager.instance.ChangePlayerTurn(Turn);
        }

        private void Update()
        {
            if (mainBallRb.velocity == Vector2.zero && !turnWasChangedAlready)
            {
                ChangePlayerTurn();
            }
        }

        private void ChangePlayerTurn()
        {
            if (additionalTurnForCurrentPlayer)
            {
                additionalTurnForCurrentPlayer = false;
                turnWasChangedAlready = true;
                IsCurrentPlayerAlreadyHitBall = false;
                return;
            }
            
            Turn = (Turn == PlayerTurn.FirstPlayer) ? PlayerTurn.SecondPlayer : PlayerTurn.FirstPlayer;
            turnWasChangedAlready = true;
            IsCurrentPlayerAlreadyHitBall = false;
            UIManager.instance.ChangePlayerTurn(Turn);
        }

        public void OnCurrentPlayerHitBall()
        {
            turnWasChangedAlready = false;
            IsCurrentPlayerAlreadyHitBall = true;
        }

        public void OnCurrentPlayerHitTheHole()
        {
            additionalTurnForCurrentPlayer = true;
        }
    }

    public enum PlayerTurn
    {
        FirstPlayer,
        SecondPlayer
    }
}
