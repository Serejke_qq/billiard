using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager instance { get; private set; }

        [SerializeField] private Text playerTurnText;
        [SerializeField] private Text firstPlayerScores;
        [SerializeField] private Text secondPlayerScores;
        [SerializeField] private Text playerTouchForceText;

        private void Awake()
        {
            if (instance == null)
                instance = this;
        }

        public void ChangePlayerTurn(PlayerTurn changeTo)
        {
            playerTurnText.text = (changeTo == PlayerTurn.FirstPlayer) ? "First player" : "Second player";
        }

        public void AddScoresToPlayer(PlayerTurn player, int newVal)
        {
            if (player == PlayerTurn.FirstPlayer)
                firstPlayerScores.text = newVal.ToString();
            else
                secondPlayerScores.text = newVal.ToString();
        }

        public void ChangePlayerForceTouch(float value)
        {
            playerTouchForceText.text = value.ToString("N1");
        }
    }
}