using UnityEngine;

namespace Assets.Scripts
{
    public class Pocket : MonoBehaviour
    {
        [SerializeField] private TurnResolver turnResolver;

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Ball"))
            {
                Destroy(other.gameObject);
                GameRestarter.instance.RemoveOneBallFromTable();
                ScoreManager.instance.AddScoreToCurrentPlayer();
                turnResolver.OnCurrentPlayerHitTheHole();
            }
        }
    }
}
